<?php namespace CZ\Http;

class CZHttp{

    const GET_METHOD            = 0;//GET Method
    const POST_METHOD           = 1;//POST Method
    const CONNECTION_TIMEOUT    = 15;
    const REQUEST_TIMEOUT       = 30;

    protected $curl;
    protected $url;
    protected $query_string;
    protected $post_fields;
    protected $method;

    protected $request;
    protected $curl_info;
    protected $response_status;
    protected $response_message;

    public function __construct($url="")
    {
        if(!empty($url)) $this->setUrl($url);
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setRequestType($method)
    {
        $this->method = $method;
    }
    public function getRequestType()
    {
        return ($this->method == 0) ? "GET" : "POST";
    }

    public function setPostData($post_data = array())
    {
        $this->post_fields = http_build_query($post_data, '', '&');
    }
    public function getPostData()
    {
        return $this->post_fields;
    }

    protected function execGET(){
        $rs = null;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_POST, self::GET_METHOD);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::CONNECTION_TIMEOUT);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::REQUEST_TIMEOUT);

        $rs = curl_exec($curl);
        curl_close ($curl);

        return $rs;
    }

    protected function execPOST()
    {
        $rs = null;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_POST, self::POST_METHOD);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->post_fields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::CONNECTION_TIMEOUT);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::REQUEST_TIMEOUT);
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow_location);

        $rs = curl_exec($curl);
        curl_close ($curl);

        return $rs;
    }

    public function send($method = self::GET_METHOD)
    {
        if($method == self::POST_METHOD) {
            $this->request = $this->execPOST();
        }else{
            $this->request = $this->execGET();
        }
        return $this;
    }

    public function response()
    {
        return $this->request;
    }

}