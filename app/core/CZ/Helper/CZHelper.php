<?php namespace CZ\Helper;

class CZHelper{

    const DATA_TYPE_STR         = 1;
    const DATA_TYPE_INT         = 2;
    const DATA_TYPE_EMAIL       = 3;
    const OPTION_SELECTED       = " selected";
    const OPTION_CHECKED        = " checked";

    public function __construct(){
        //Empty Constructor
    }

    /**
     * Function to sanitize input value or variable
     * @param $input
     * @param int $return_type
     * @param null $custom_return_value
     * @param bool|true $clear_symbols
     * @return int|null|string
     */
    public static function SanitizeValue(
        $input,
        $return_type=self::DATA_TYPE_STR,
        $custom_return_value=null,
        $clear_symbols=true)
    {
        if(!(is_null($input) && empty($input))){
            $input = trim($input);
            if($return_type === self::DATA_TYPE_STR) {
                return htmlspecialchars(stripslashes($input));
            }
            elseif($return_type == self::DATA_TYPE_INT){
                $pattern = '([\,|\.])';
                return ($clear_symbols) ? intval(preg_replace($pattern,'',$input)) : intval($input);
            }
        }
        return (!(is_null($custom_return_value) && empty($custom_return_value))) ? $custom_return_value : null;
    }

    /**
     * @param string $key
     * @param string $value
     * @param bool|false $selected
     * @return string
     */
    public static function generateOptions($key, $value, $selected=false){
        $is_selected = (!is_null($selected) && $selected == true) ? ' selected' : '';
        return '<option value="' . $key .'"'.$is_selected.'>'.$value.'</option>';
    }

    /**
     * @param string $input_name
     * @param mixed $default_value
     * @param string|int $index
     * @param string $name_prefix
     * @return mixed
     */
    public static function setInputValue($input_name, $default_value=null, $index=null, $name_prefix='txt')
    {
        $result = $default_value;
        $post_value = null;
        if(!(is_null($name_prefix) && empty($name_prefix))){
            if (isset($_POST[$name_prefix][$input_name])) $post_value = $_POST[$name_prefix][$input_name];
        }
        else{
            if (isset($_POST[$input_name])) $post_value = $_POST[$input_name];
        }
        if(!is_null($post_value)) $result = (is_null($index)) ? $post_value : $post_value[$index];

        return $result;
    }

    /**
     * @param string $input_name
     * @param string $value
     * @param mixed $default_value [Default: null]
     * @param string $flag
     * @param string|int $index
     * @param string $name_prefix
     * @return null|string
     */
    public static function setSelected($input_name, $value, $default_value=null, $flag=self::OPTION_CHECKED, $index=null, $name_prefix='txt')
    {
        $post_value = self::setInputValue($input_name, $default_value, $index, $name_prefix);
        if(!is_null($post_value) && $post_value == $value) return $flag;

        return null;
    }

    /**
     * Function to insert CSS file(s)
     *
     * @param array|string $file_name
     * @return bool|string
     */
    public static function InsertCSS($file_name=null){

        $css_start = '<link href="';
        $css_end = '" rel="stylesheet">';

        if(!is_null($file_name)){
            $css_file = "";
            if(is_array($file_name) && count($file_name) > 0){
                foreach($file_name as $file){
                    $tmp = $css_start . $file . $css_end . "\n";
                    $css_file .= $tmp;
                }
            }
            else{
                if(!empty($file_name)) $css_file = $css_start . $file_name . $css_end . "\n";
            }
            return $css_file;
        }
        return null;
    }

    /**
     * Echo InsertCSS
     *
     * @param string|array $file_name
     */
    public static function RenderCSS($file_name=null){
        echo self::InsertCSS($file_name);
    }

    /**
     * Function to insert Javascript file(s)
     *
     * @param string $file_name
     * @return bool|string
     */
    public static function InsertJS($file_name=null){

        $start_script = '<script src="';
        $end_script = '"></script>';

        if(!is_null($file_name)){
            $js_script = "";
            if(is_array($file_name) && count($file_name) > 0){
                foreach($file_name as $file){
                    $tmp = $start_script . $file . $end_script . "\n";
                    $js_script .= $tmp;
                }
            }
            else{
                if(!empty($file_name)) $js_script = $start_script . $file_name . $end_script . "\n";
            }
            return $js_script;
        }
        return null;
    }

    /**
     * Echo InsertJS
     *
     * @param string|array $file_name
     */
    public static function RenderJS($file_name=null){
        echo self::InsertJS($file_name);
    }

    public static function JSAlert($message){
        $script_start = "<script>";
        $script_end = "</script>";
        return $script_start . "alert('$message')" . $script_end;
    }

    public static function ShowJSAlert($message){
        echo self::JSAlert($message);
    }

    /**
     * Test variable
     *
     * @param $test_variable
     * @param bool|false $exit
     */
    public static function RunDebug($test_variable, $exit = false){
        echo "<pre>";
        if(is_array($test_variable))
        {
            print_r($test_variable);
        }
        else{
            echo $test_variable;
        }
        echo "</pre>";

        if($exit) exit;
    }
}